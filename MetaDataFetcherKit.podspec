Pod::Spec.new do |s|
  s.name             = "MetaDataFetcherKit"
  s.version          = "0.4.0"
  s.summary          = "An ObjC library to fetch meta data for media content"

  s.homepage         = "https://code.videolan.org/fkuehne/MetaDataFetcherKit"
  s.license          = 'LGPL v21'
  s.author           = { "Felix Paul Kühne" => "fkuehne@videolan.org" }
  s.source           = { :git => "https://code.videolan.org/fkuehne/MetaDataFetcherKit.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/feepk'

  s.tvos.deployment_target = '9.0'
  s.ios.deployment_target = '9.0'
  s.osx.deployment_target = '10.9'

  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'

  s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'Foundation'

  s.dependency 'AFNetworking', '3.1.0'

end
